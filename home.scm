(use-modules
 (gnu home)
 (gnu packages)
 (gnu services)
 (gnu home services dotfiles)
 (gnu home services desktop)
 (gnu home services sound))


(home-environment
 (packages (specifications->packages
            '("sx" "xfce" "herbstluftwm" "rofi" "pavucontrol"
              "font-sarasa-gothic" "font-lxgw-wenkai" "font-recursive" "font-google-noto-emoji"
              "fcitx5" "fcitx5-gtk:gtk3" "fcitx5-rime" "fcitx5-qt"
              "ungoogled-chromium" "qt5ct" "breeze-icons"
              "emacs" "emacs-paredit" "emacs-magit" "emacs-elpher"
              "emacs-pyim" "emacs-pyim-basedict"
              "emacs-ef-themes" "emacs-debbugs" "emacs-guix"
              "emacs-vertico" "emacs-consult" "emacs-orderless" "emacs-editorconfig"
              "tmux" "gnupg" "password-store" "git" "git:send-email"
              "ugrep" "drawterm" "fdm" "rizin")))
 (services (list
            (service home-dbus-service-type)
            (service home-pipewire-service-type)
            (service home-dotfiles-service-type
                     (home-dotfiles-configuration
                      (directories '("./dotfiles")))))))
