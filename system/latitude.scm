(use-modules
 (gnu)
 (gnu services networking)
 (gnu services ssh)
 (gnu services dbus)
 (gnu services desktop)
 (gnu packages ssh)
 (gnu packages package-management)
 (gnu packages fonts)
 (gnu packages tmux)
 (gnu packages terminals)
 (nongnu packages linux)
 (nongnu system linux-initrd))

(define* (%tmpfs mount-point #:key
                 (size "1G")
                 (flags '(no-dev no-suid no-exec)))
  (file-system
    (device "none")
    (mount-point mount-point)
    (type "tmpfs")
    (needed-for-boot? #t)
    (create-mount-point? #t)
    (options (string-append "size=" size))
    (flags flags)))

(operating-system
 (host-name "latitude")
 (timezone "Asia/Shanghai")
 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (keyboard-layout (keyboard-layout "us" "dvorak"))
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets '("/boot"))))

 (file-systems (cons* (file-system
                       (device "/dev/sda1")
                       (mount-point "/boot")
                       (type "vfat"))
                      (file-system
                       (device "/dev/sda3")
                       (options "compress=zstd,subvol=@Guix")
                       (mount-point "/")
                       (type "btrfs"))
                      (file-system
                       (device "/dev/sda3")
                       (options "compress=zstd,subvol=@Home")
                       (mount-point "/home")
                       (type "btrfs"))
                      (%tmpfs "/tmp")
                      (%tmpfs "/var/tmp")
                      (%tmpfs "/run")
                      (%tmpfs "/var/run")
                      (%tmpfs "/run/setuid-programs" #:flags '(no-dev))
                      %base-file-systems))

 (users (cons (user-account
               (name "iyzsong")
               (group "users")
               (supplementary-groups '("wheel" "audio" "video" "input" "netdev")))
              %base-user-accounts))

 (packages (cons* tmux foot %base-packages))

 (services (append (list (service dbus-root-service-type)
                         (service elogind-service-type)
                         (service wpa-supplicant-service-type)
                         (service network-manager-service-type))
                   (modify-services %base-services
                                    (console-font-service-type
                                     config =>
                                     (map (lambda (x)
                                            (cons (car x)
                                                  (file-append (gexp-input font-spleen "psf")
                                                               "/share/consolefonts/spleen-16x32.psfu")))
                                          config))
                                    (guix-service-type
                                     config =>
                                     (guix-configuration
                                      (inherit config)
                                      (guix (current-guix))
                                      (substitute-urls '("https://mirror.sjtu.edu.cn/guix/"))))))))
