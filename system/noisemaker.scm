(use-modules
 (gnu)
 (guix utils)
 (guix packages))

(use-service-modules networking ssh dbus dict mail web
		     file-sharing xorg desktop admin virtualization linux)
(use-package-modules screen fonts dico file linux)

(define my/ssh-public-key (local-file "files/keys/iyzsong.pub"))

(define* (%tmpfs mount-point #:key
                 (size "1G")
                 (flags '(no-dev no-suid no-exec)))
  (file-system
    (device "none")
    (mount-point mount-point)
    (type "tmpfs")
    (needed-for-boot? #t)
    (create-mount-point? #t)
    (options (string-append "size=" size))
    (flags flags)))

(operating-system
  (host-name "noisemaker")
  (timezone "Asia/Shanghai")
  (locale "en_US.utf8")
  (kernel-arguments '("video=eDP-1:d" "quiet"))
  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets '("/boot/efi"))))
  (file-systems (cons*
                 (file-system
                   (device (file-system-label "EFI"))
                   (mount-point "/boot/efi")
                   (type "vfat"))
                 (file-system
                   (device (file-system-label "GUIX"))
                   (mount-point "/")
                   (type "ext4"))
                 (%tmpfs "/tmp")
                 (%tmpfs "/var/tmp")
                 (%tmpfs "/run")
                 (%tmpfs "/var/run")
                 (%tmpfs "/run/setuid-programs" #:flags '(no-dev))
                 %base-file-systems))
  (keyboard-layout (keyboard-layout "us" "dvorak"))

  (users (cons (user-account
                (name "iyzsong")
                (group "users")
                (supplementary-groups '("wheel" "audio" "video" "input" "dialout" "tty" "kvm")))
               %base-user-accounts))

  (packages (cons* file
                   screen
                   iptables
                   %base-packages))

  (services (append (list (service dhcp-client-service-type)
                          (service dbus-root-service-type)
                          (service elogind-service-type)
                          (service fstrim-service-type)
                          (service zram-device-service-type
                                   (zram-device-configuration
                                    (size "4G")
                                    (compression-algorithm 'zstd)
                                    (memory-limit "2G")))
                          (service gvfs-service-type)
			  (service opensmtpd-service-type
				   (opensmtpd-configuration
				    (config-file (local-file "files/smtpd.conf"))))
                          (service xorg-server-service-type
                                   (xorg-configuration
                                    (keyboard-layout keyboard-layout)))
                          (service openssh-service-type
                                   (openssh-configuration
                                    (permit-root-login 'prohibit-password)
                                    (password-authentication? #f)
                                    (authorized-keys `(("root" ,my/ssh-public-key)
                                                       ("iyzsong" ,my/ssh-public-key))))))
                    (modify-services %base-services
                      (guix-service-type config =>
                                         (guix-configuration
                                          (inherit config)
                                          (substitute-urls
                                           '("https://mirror.sjtu.edu.cn/guix/"
                                             "https://bordeaux-singapore-mirror.cbaines.net"))))
                      (console-font-service-type
                       config =>
                       (map (lambda (x)
                              (cons (car x)
                                    (file-append font-terminus "/share/consolefonts/ter-132b")))
                            config))))))
