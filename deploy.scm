(use-modules
 (gnu)
 (gnu machine)
 (gnu machine ssh)
 (guix gexp)
 (guix store)
 (guix grafts)
 (guix profiles)
 (gnu packages base)
 (gnu packages video)
 (gnu packages package-management))

(define vf2
  (machine
   (operating-system #f)
   (environment managed-host-environment-type)
   (configuration
    (machine-ssh-configuration
     (host-name "192.168.1.4")
     (system "riscv64-linux")
     (build-locally? #f)
     (user "root")
     (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDdBQ74K/gtggHkmnhxrHK8EZUKbFL6r8fKY53jibvQ/")))))

(define %profile
  (profile
   (name "default")
   (content (packages->manifest
	     (list ffmpeg glibc-locales guix-build-coordinator/agent-only)))
   (hooks '())))

(parameterize ((%graft? #f))
  (with-store %store
    (run-with-store %store
      (machine-remote-eval
       vf2
       #~(let ((dest "/var/guix/profiles/default"))
	   (false-if-exception (delete-file dest))
	   (symlink #$%profile dest))))))
