;;; -*- lexical-binding: t -*-
(setq package-archives
      '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
        ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")))
(package-initialize)
(setq custom-file "~/.emacs.d/custom.el")
(load-file custom-file)

(use-package emacs
  :custom
  (auto-save-default nil)
  (indent-tabs-mode nil)
  :bind
  (("C-z" . eshell)
   ("<f12>" . bs-show))
  :hook
  (after-save . delete-trailing-whitespace))

(use-package paredit
  :hook
  ((scheme-mode emacs-lisp-mode clojure-mode) . enable-paredit-mode))

(use-package rcirc
  :hook (rcirc-mode . rcirc-omit-mode))

(use-package pyim
  :custom (default-input-method "pyim")
  :config (pyim-basedict-enable))

(use-package dired-x
  :custom
  (dired-omit-files "^[.]")
  :hook
  (dired-mode . dired-omit-mode)
  (dired-mode . dired-hide-details-mode))

(use-package vertico
  :init (vertico-mode))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completing-category-overrides '((file (styles partial-completion)))))

(use-package consult
  :bind
  (("C-x b" . consult-buffer)))
